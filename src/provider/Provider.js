import React, {createContext, useState} from 'react';

export const CalculatorContext = createContext();
const {Provider} = CalculatorContext;

export const CalculatorProvider = ({children}) => {
  const [number, setNumber] = useState('')
  const [prevNumber, setPrevNumber] = useState('')
  const [operator, setOperator] = useState('')
  const [display, setDisplay] = useState('')

  const handleNumberClick = value => {
    setNumber(`${number}${value}`)
    setDisplay(number)
  }
  const handleOperatorClick = value => {
    if(operator === value) {
      setNumber('')
      return
    }
    setPrevNumber(number)
    setOperator(value)
    setDisplay(prevNumber)
    setNumber('')
  }

  const doOperation = type => {
    switch(type) {
      case '+':
        setNumber(Math.floor(parseFloat(prevNumber) + parseFloat(number)))
        break
      case '-':
          setNumber(Math.floor(parseFloat(prevNumber) - parseFloat(number)))
        break
      case '/':
          setNumber(Math.floor(parseFloat(prevNumber) / parseFloat(number)))
        break
      case '*':
          setNumber(Math.floor(parseFloat(prevNumber) * parseFloat(number)))
        break
      default:
        break
    }
    setOperator('')
    setPrevNumber('')
  }

  return(
    <Provider value={{
      handleNumberClick,
      handleOperatorClick,
      doOperation,
      operator,
      number,
      setNumber,
    }}>
      {children}
    </Provider>
  )
}

