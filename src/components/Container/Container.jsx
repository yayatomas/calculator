import React, {useContext} from 'react';
import {numbersArray, operatorsArray} from './containerUtils';
import {Button} from '../Button/Button';
import {CalculatorContext} from '../../provider/Provider';

export const Container = () => {
  const containerContext = useContext(CalculatorContext)
  const {
    handleNumberClick,
    handleOperatorClick,
    doOperation,
    operator,
    setNumber,
    number,
  } =  containerContext;
  const numbersButtons = numbersArray.map((num, i) => <Button 
        style={styles.button} 
        key={i} value={num} 
        onClick={() => handleNumberClick(num)}
      />
    );
  const operatorsButtons = operatorsArray.map((op, i) => <Button 
        style={styles.button }  
        key={i} 
        value={op} 
        onClick={() => handleOperatorClick(op)}
      />
    )
  return(
    <div style={styles.container}>
      <input type='text' readOnly value={number} />
      <div>
        {operatorsButtons}
      </div>
      <div style={styles.numberContainer}>
        {numbersButtons}
      </div>
      <div>
        <button style={styles.button} type="button" onClick={() => doOperation(operator)}>
          Go
        </button>
        <button style={styles.button} type="button" onClick={() => setNumber('')}>
          Cc
        </button>
      </div>
    </div>
  )
};

const styles = {
  container: {
    display: 'flex',
    flexDirection: 'column',
    width: '200px',
    margin: '30px auto',
    alignItems: 'center',
    backgroundColor: '#303030',
    borderRadius: '10px',
  },
  numberContainer: {
    display: 'flex',
    flexWrap: 'wrap',
    width: '100px',
    margin: '10px auto'
  },
  button: {
    margin: 1,
    border: 'none',
    borderRadius: '2px'
  },
}

