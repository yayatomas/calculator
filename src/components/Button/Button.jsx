import React from 'react';

export const Button = ({value, ...props}) => {
    return(
      <button type='button' {...props}>
        {value}
      </button>
    )
}