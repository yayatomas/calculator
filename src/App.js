import React from 'react';
import {CalculatorProvider} from './provider/Provider';
import {Container} from './components/Container/Container';


function App() {
  return (
    <CalculatorProvider>
      <Container />
    </CalculatorProvider>
  );
}

export default App;
